package com.rental.management.messageservices.repository;

import com.rental.management.messageservices.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findMessageByGroupEquals(Long groupId);
}
