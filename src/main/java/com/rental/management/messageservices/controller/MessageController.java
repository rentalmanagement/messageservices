package com.rental.management.messageservices.controller;

import com.rental.management.messageservices.model.Group;
import com.rental.management.messageservices.model.Message;
import com.rental.management.messageservices.model.User;
import com.rental.management.messageservices.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class MessageController {

    @Autowired
    MessageService messageService;


    /**
     *  Add a message (saved in the group that is referenced in Message object)
     * @param message - message to be saved
     * @return saved message
     */
    @PostMapping("/messages")
    @ResponseStatus(HttpStatus.CREATED)
    public Message save(@RequestBody Message message) {
        return messageService.save(message);
    }

    /**
     * Get a message by id
     * @param id - message id
     * @return message with specified id
     */
    @GetMapping("/messages/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Message getMessage(@PathVariable Long id) {
        return messageService.getMessage(id);
    }


    /**
     * Get a message by id
     * @param id - message id
     * @return message with specified id
     */
    @GetMapping("/groups/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Group getGroup(@PathVariable Long id) {
        return messageService.getGroup(id);
    }

    /**
     * Get all messages for a group
     * @param groupId - groupId group's id
     * @return - list of messages
     */
    @GetMapping("/messages/groups/{groupId}")
    @ResponseStatus(HttpStatus.OK)
    public List<Message> getMessagesForGroup(@PathVariable Long groupId) {
        return messageService.getMessagesForGroup(groupId);
    }

    /**
     * Get all messages for a group
     * @param groupId - groupId group's id
     * @return - list of messages
     */
    @GetMapping("/messages/groups/{groupId}/users")
    @ResponseStatus(HttpStatus.OK)
    public List<User> getUsersForGroup(@PathVariable Long groupId) {
        return messageService.getUsersForGroup(groupId);
    }


    @PatchMapping("/messages")
    @ResponseStatus(HttpStatus.OK)
    public Message update(@RequestBody Message message) {
        return messageService.update(message);
    }

    @DeleteMapping("/messages/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        messageService.delete(id);
    }

    /**
     * Create a new group
     * @param group - group to be created
     */
    @PostMapping("/messages/groups")
    @ResponseStatus(HttpStatus.CREATED)
    public void createNewGroup(@RequestBody Group group) {
        messageService.createNewGroup(group);
    }

    /**
     * Add a new member to an existing group
     * @param user - member to be added
     * @param groupId - group's id where the member will be added
     */
    @PostMapping("/messages/groups/{groupId}/users")
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewMemberToGroup(@RequestBody User user, @PathVariable Long groupId) {
        messageService.addNewMemberToGroup(user, groupId);
    }

    @DeleteMapping("/messages/groups/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGroup(@PathVariable Long groupId) {
        messageService.deleteGroup(groupId);
    }
}
