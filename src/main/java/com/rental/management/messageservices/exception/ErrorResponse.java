package com.rental.management.messageservices.exception;

public class ErrorResponse {
    private String cause;
    private String code;
    private String className;

    public ErrorResponse(String cause, String code, String className) {
        this.cause = cause;
        this.code = code;
        this.className = className;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
