package com.rental.management.messageservices.service;

import com.rental.management.messageservices.model.Group;
import com.rental.management.messageservices.model.Message;
import com.rental.management.messageservices.model.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface MessageService {

    Message save(Message message);

    Message getMessage(Long id);

    List<Message> getMessagesForGroup(Long id);

    void delete(Long id);

    Message update(Message message);

    void addNewMemberToGroup(User user, Long groupId);

    void deleteGroup(Long groupId);

    void createNewGroup(Group group);

    List<User> getUsersForGroup(Long groupId);

    Group getGroup(Long id);
}
