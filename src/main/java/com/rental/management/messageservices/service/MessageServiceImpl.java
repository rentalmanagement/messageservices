package com.rental.management.messageservices.service;

import com.rental.management.messageservices.exception.ResourceNotFoundException;
import com.rental.management.messageservices.model.Group;
import com.rental.management.messageservices.model.Message;
import com.rental.management.messageservices.model.User;
import com.rental.management.messageservices.repository.GroupRepository;
import com.rental.management.messageservices.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageRepository repository;
    @Autowired
    GroupRepository groupRepository;

    @Override
    @Transactional
    public Message save(Message message) {
        return repository.save(message);
    }

    @Override
    public Message getMessage(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public List<Message> getMessagesForGroup(Long id) {
        return repository.findMessageByGroupEquals(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public Message update(Message updatedMessage) {
        Message message = repository.findById(updatedMessage.getId()).orElseThrow(ResourceNotFoundException::new);
        message.setMessage(updatedMessage.getMessage());
        message.setSubject(updatedMessage.getSubject());
        return repository.save(message);
    }

    @Override
    public void addNewMemberToGroup(User user, Long groupId) {

        Optional<Group> group = groupRepository.findById(groupId);
        group.ifPresent(group1 -> group1.getGroupUsers().add(user));
        groupRepository.save(group.get());

    }

    @Override
    public void deleteGroup(Long groupId) {
        groupRepository.deleteById(groupId);
    }

    @Override
    public void createNewGroup(Group group) {
        System.out.println("Grup: " + group);
        groupRepository.save(group);
    }

    @Override
    public List<User> getUsersForGroup(Long groupId) {
        Optional<Group> group = groupRepository.findById(groupId);
        return group.map(Group::getGroupUsers).orElse(null);
    }

    @Override
    public Group getGroup(Long groupId) {
        Optional<Group> group = groupRepository.findById(groupId);
        return group.orElse(null);
    }
}
