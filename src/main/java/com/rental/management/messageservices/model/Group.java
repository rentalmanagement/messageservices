package com.rental.management.messageservices.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity(name = "`group`")
//@Getter
//@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long groupId;
    @NotNull(message = "{group.groupName.notNull}")
    private String groupName;
    @NonNull
    @ManyToMany( cascade = CascadeType.MERGE)
    @JoinTable(
            name = "Group_User",
            joinColumns = { @JoinColumn(name = "group_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private List<User> groupUsers;
}
