package com.rental.management.messageservices.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "{message.timestamp.notNull}")
    @Column(name = "receivedAt")
    private Timestamp timestamp;
    @NotNull(message = "{message.from.notNull}")
    @Column(name = "`from`")
    private Long from;
    @NotNull
    @Column(name = "`fromName`")
    private String fromName;
    @Column(name = "`to`")
    private Long to;
    @Column(name = "`subject`")
    private String subject;
    @NotNull(message = "{message.message.notNull}")
    private String message;
    @NotNull(message = "{message.group.notNull}")
    @Column(name = "`id_group`")
    private Long group;
}
