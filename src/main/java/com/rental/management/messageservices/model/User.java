package com.rental.management.messageservices.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
public class User {
    @Id
    private Long id;

    @NotNull
    private String username;

    @JsonIgnore
    @ManyToMany(mappedBy = "groupUsers", cascade = CascadeType.MERGE )
    private List<Group> groups;
}
