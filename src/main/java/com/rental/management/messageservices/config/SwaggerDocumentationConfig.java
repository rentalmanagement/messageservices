package com.rental.management.messageservices.config;

import com.rental.management.messageservices.MessageServicesApplication;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;

@Configuration
@EnableSwagger2
public class SwaggerDocumentationConfig {
    @Value("${document.contact-email}")
    private String contactEmail;

    @Bean
    public Docket defaultApi() {
        Parameter p = new ParameterBuilder()
                .name("Authorization")
                .description("Bearer \"access-token\"")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(true)
                .build();

        List<Parameter> paramList = new ArrayList<>();
        paramList.add(p);

        Set<String> producesSet = new HashSet<String>();
        producesSet.add("application/json");

        return new Docket(DocumentationType.SWAGGER_2).groupName("messageServices")
                .select()
                .apis(RequestHandlerSelectors.basePackage(MessageServicesApplication.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .globalOperationParameters(paramList)
                .pathMapping("/")
                .produces(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON.toString())))
                .consumes(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON.toString())))
                .protocols(new HashSet<>(Collections.singletonList("https")))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {

        ApiInfo apiInfo = new ApiInfo("MessageServices REST Services",
                "Order REST Services for UserServices. ",
                "1.0",
                "",
                new Contact("IT", "", contactEmail),
                "",
                "");
        return apiInfo;
    }
}
